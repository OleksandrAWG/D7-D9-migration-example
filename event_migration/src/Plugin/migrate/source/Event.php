<?php

/**
 * @file
 * Contains \Drupal\event_migration\Plugin\migrate\source\Fruit.
 */

namespace Drupal\event_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * Drupal 7
 *
 * @MigrateSource(
 *   id = "event_node"
 * )
 */
class Event extends Node {
  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $nid = $row->getSourceProperty('nid');

    //Teaser Title field
    $field_teaser_title = $this->select('field_data_field_teaser_title', 't')
      ->fields('t', ['field_teaser_title_value'])
      ->condition('t.entity_id', $nid)
      ->execute()->fetchField();
    if (!empty($field_teaser_title)) {
      $row->setSourceProperty('teaser_title', $field_teaser_title);
    }

    //Date Time field
    $field_date_time = $this->select('field_data_field_date_time', 'd')
      ->fields('d', ['field_date_time_value'])
      ->condition('d.entity_id', $nid)
      ->execute()->fetchField();
    if (!empty($field_date_time)) {
      $row->setSourceProperty('date_time', $field_date_time);
    }

    //Private Event field
    $field_private_event = $this->select('field_data_field_private_event', 'p')
      ->fields('p', ['field_private_event_value'])
      ->condition('p.entity_id', $nid)
      ->execute()->fetchField();
    if (!empty($field_private_event)) {
      $row->setSourceProperty('private_event', $field_private_event);
    }

    //Organisation field
    $field_organisation = $this->select('field_data_field_organisation', 'o')
      ->fields('o', ['field_organisation_value'])
      ->condition('o.entity_id', $nid)
      ->execute()->fetchField();
    if (!empty($field_organisation)) {
      $row->setSourceProperty('organisation', $field_organisation);
    }

     //Location Terms Reference
    $location_terms = [];
    $result = $this->select('field_data_field_location', 'fdv')
      ->fields('fdv', ['field_location_tid'])
      ->condition('fdv.entity_id', $nid)
      ->execute();
    while($record = $result->fetchObject()){
      $location_terms[] = $record->field_location_tid;
    }
    if (!empty($location_terms)) {
      $row->setSourceProperty('location_terms', $location_terms);
    }

    // Image field
    $images = [];
    $result = $this->select('field_data_field_banner_image', 'fdi')
      ->fields('fdi', ['field_banner_image_fid', 'field_banner_image_alt', 'field_banner_image_title', 'field_banner_image_width', 'field_banner_image_height'])
      ->condition('fdi.entity_id', $nid)
      ->execute();
    while($record = $result->fetchObject()){
      $images[] = [
        'target_id' => $record->field_banner_image_fid,
        'alt' => $record->field_banner_image_alt,
        'title' => $record->field_banner_image_title,
        'width' => $record->field_banner_image_width,
        'height' => $record->field_banner_image_height,
      ];
    }
    if (!empty($images)) {
      $row->setSourceProperty('banner_image', $images);
    }
    // Migrate URL alias.
    $alias = $this->select('url_alias', 'ua')
      ->fields('ua', ['alias'])
      ->condition('ua.source', 'node/' . $nid)
      ->execute()
      ->fetchField();
    if (!empty($alias)) {
      $row->setSourceProperty('alias', '/' . $alias);
    }
    return parent::prepareRow($row);
  }
}
